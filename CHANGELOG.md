# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- 

### Changed

- 

### Removed

- 

## [1.4.1] - 2024-03-28

### Added

- store created factory based on the model and provided attrs

## [1.4.0] - 2024-03-27

### Added

- handle cyclic relationships

## [1.3.0] - 2024-03-04

### Added

- handle generation of sqlalchemy class with relationship but no back_populate
- handle bytes natively for generation of factory attribute
