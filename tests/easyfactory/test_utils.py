from datetime import datetime
from uuid import UUID, uuid4

import factory
import pytest

from easyfactory.utils import select_generator_for_type


@pytest.mark.parametrize("type_,expected_provider", [
    (bytes, "binary"),
    (int, "pyint"),
    (float, "pyfloat"),
    (bool, "pybool"),
    (datetime, "date_time"),
])
def test_select_generator_for_type_should_return_appropriate_factory_for_basic_type(type_, expected_provider):
    # when
    result = select_generator_for_type("name", type_, lambda x: x)
    # then
    assert result.provider == expected_provider


def test_select_generator_for_type_should_return_appropriate_factory_for_uuid():
    # when
    result = select_generator_for_type("name", UUID, lambda x: x)
    # then
    assert isinstance(result, factory.LazyFunction)
    assert result.function == uuid4
