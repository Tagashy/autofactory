from typing import Any

import pytest

from easyfactory import AutoFactory
from easyfactory.auto_factory import make_factory_for
from easyfactory.sqlalchemy import load_type, make_post_gen
from tests.test_data.models import HostSQLAlchemyModel, ResponseSQLAlchemyModel, RequestSQLAlchemyModel, Base


def test_load_type_should_raise_if_unknown_name():
    with pytest.raises(TypeError):
        load_type(Base._sa_registry.mappers, "Host")


def test_load_type_should_return_valid_type():
    assert load_type(Base._sa_registry.mappers, "RequestSQLAlchemyModel") == RequestSQLAlchemyModel


def test_make_post_gen_should_generate_a_model_without_property_linked(mocker):
    factory = mocker.Mock()
    post_gen = make_post_gen("host", factory, None, None, None)

    class MyModel:
        host: str = None

    my_model = MyModel()
    # when
    post_gen(my_model, Any, None)
    # then
    assert my_model.host == factory.return_value


def test_auto_factory_should_generate_a_factory_based_on_sql_model():
    # given

    # when
    class TestFactory(AutoFactory):
        class Meta:
            model = RequestSQLAlchemyModel

    model: RequestSQLAlchemyModel = TestFactory()
    # then
    assert model.host.requests == [model]
    assert model.response.request == model
    assert len(model.vulnerabilities) == 1
    assert model.vulnerabilities[0].request == model


def test_make_factory_for_should_generate_a_factory_based_on_sql_model():
    # given

    # when
    SimpleModelFactory = make_factory_for(RequestSQLAlchemyModel)
    model = SimpleModelFactory()
    # then
    assert model.host.requests == [model]
    assert model.response.request == model
    assert len(model.vulnerabilities) == 1
    assert model.vulnerabilities[0].request == model


def test_generated_factories_should_allow_override_of_relationships():
    # given
    HostFactory = make_factory_for(HostSQLAlchemyModel)
    # when
    host = HostFactory(requests=[])
    # then
    assert host.requests == []


def test_generated_factories_should_allow_override_of_relationships_nested():
    # given
    HostFactory = make_factory_for(HostSQLAlchemyModel)
    ResponseFactory = make_factory_for(ResponseSQLAlchemyModel)
    host = HostFactory(requests=[])
    # when
    response = ResponseFactory(request__host=host)
    # then
    assert response.request.host == host
