from factory import Faker
from pydantic import BaseModel

from easyfactory.pydantic import PydanticFactoryGenerator
from tests.matchers import Any_

MODULE_PACKAGE = "easyfactory.pydantic"


def test_make_pydantic_factory_should_generate_only_once_a_factory_for_a_given_model(mocker):
    # given
    class SimpleModel(BaseModel):
        name: str
        level: int

    make_factory_class_patch = mocker.patch(f"{MODULE_PACKAGE}.make_factory_class")
    # when
    PydanticFactoryGenerator.make_factory_for(SimpleModel)
    PydanticFactoryGenerator.make_factory_for(SimpleModel)
    # then
    make_factory_class_patch.assert_called_once_with(SimpleModel, {'level': Any_(Faker), 'name': Any_(Faker)})
