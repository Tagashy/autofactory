from datetime import datetime
from typing import Optional
from uuid import UUID, uuid4

import faker
import pytest
from pydantic import BaseModel

from easyfactory.auto_factory import AutoFactory, make_factory_for
from tests.matchers import Any_


def test_auto_factory_should_generate_a_factory_based_on_model():
    # given
    class SimpleModel(BaseModel):
        id: UUID | None = None
        name: str
        level: int
        status: str | None
        created_at: datetime = datetime(2020, 12, 16)

    # when
    class TestFactory(AutoFactory):
        class Meta:
            model = SimpleModel

    model: SimpleModel = TestFactory()
    # then
    assert model.name == Any_(str)
    assert model.level == Any_(int)
    assert model.created_at == datetime(2020, 12, 16)


def test_make_factory_for_should_generate_a_factory_based_on_model():
    # given
    class SimpleModel(BaseModel):
        id: Optional[UUID] = None
        name: str
        level: int
        status: Optional[str]
        created_at: datetime = datetime(2020, 12, 16)

    # when
    SimpleModelFactory = make_factory_for(SimpleModel)
    model = SimpleModelFactory()
    # then
    assert model.name == Any_(str)
    assert model.level == Any_(int)
    assert model.created_at == datetime(2020, 12, 16)


def test_make_factory_for_should_raise_if_unknown_type():
    # then
    with pytest.raises(TypeError):
        # when
        make_factory_for(int)


def test_auto_factory_should_generate_a_factory_based_on_complex_model():
    # given
    class SimpleModel(BaseModel):
        id: Optional[UUID] = None
        name: str
        level: int
        status: Optional[str]
        created_at: datetime = datetime(2020, 12, 16)

    class ComplexModel(BaseModel):
        simple_model: SimpleModel

    # when
    class TestFactory(AutoFactory):
        class Meta:
            model = ComplexModel

    model: ComplexModel = TestFactory()
    # then
    assert model.simple_model.name == Any_(str)
    assert model.simple_model.level == Any_(int)
    assert model.simple_model.created_at == datetime(2020, 12, 16)


def test_autofactory_should_raise_if_meta_is_not_present():
    with pytest.raises(TypeError):
        class TestFactory(AutoFactory):
            pass


def test_autofactory_should_raise_if_meta_is_present_but_empty():
    with pytest.raises(TypeError):
        class TestFactory(AutoFactory):
            class Meta:
                pass


def test_make_factory_for_should_generate_a_factory_based_on_complex_model():
    # given
    class SimpleModel(BaseModel):
        id: Optional[UUID] = None
        name: str
        level: int
        status: Optional[str]
        created_at: datetime = datetime(2020, 12, 16)

    class ComplexModel(BaseModel):
        simple_model: SimpleModel

    # when
    ComplexModelFactory = make_factory_for(ComplexModel)
    model = ComplexModelFactory()
    # then
    assert model.simple_model.name == Any_(str)
    assert model.simple_model.level == Any_(int)
    assert model.simple_model.created_at == datetime(2020, 12, 16)


def test_make_factory_for_should_generate_a_factory_with_self_attribute_on_relation():  # given
    class SimpleModel(BaseModel):
        id: Optional[UUID] = None
        name: str
        level: int
        status: Optional[str]
        created_at: datetime = datetime(2020, 12, 16)

    class ComplexModel(BaseModel):
        relation_id: UUID
        relation: Optional[SimpleModel] = None

    # when
    ComplexModelFactory = make_factory_for(ComplexModel)
    relation_id = uuid4()
    model = ComplexModelFactory(relation=make_factory_for(SimpleModel)(id=relation_id))
    # then
    assert model.relation_id == relation_id


def test_make_factory_for_should_generate_a_factory_with_a_list():
    # given
    class SimpleModel(BaseModel):
        id: Optional[UUID] = None
        tags: list[str]

    # when
    SimpleModelFactory = make_factory_for(SimpleModel)
    model = SimpleModelFactory()
    # then
    assert model.tags == [Any_(str)]


def test_make_factory_for_should_generate_a_factory_with_a_list_of_simple_model():
    # given

    class SimpleModel(BaseModel):
        id: UUID

    class ComplexModel(BaseModel):
        id: Optional[UUID] = None
        tags: list[SimpleModel]

    # when
    SimpleModelFactory = make_factory_for(ComplexModel)
    model = SimpleModelFactory()
    # then
    assert model.tags == [Any_(SimpleModel)]


def test_make_factory_for_should_generate_a_factory_with_dict_properties():
    # given

    class SimpleModel(BaseModel):
        id: UUID

    class ComplexModel(BaseModel):
        id: Optional[UUID] = None
        # id_to_model: dict[int, SimpleModel]
        # dependencies: dict[float, bool]
        metadata: dict[str, list[dict[str, datetime]]]

    faker.Faker.seed(0)  # needed to know the value generated in the following functions
    # when
    SimpleModelFactory = make_factory_for(ComplexModel)
    model = SimpleModelFactory()
    # then
    # for whatever reason there is a difference between pipeline date and local so use Any_(datetime)
    assert model.metadata == {'three': [{'image': Any_(datetime)}]}
