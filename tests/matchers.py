class Any_:
    def __init__(self, cls: type):
        self.cls = cls

    def __eq__(self, other):
        return isinstance(other, self.cls)
