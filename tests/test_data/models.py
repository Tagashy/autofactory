import enum
from datetime import datetime
from typing import Annotated, Any, Optional, Literal
from uuid import UUID, uuid4

from sqlalchemy import ForeignKey, JSON, UniqueConstraint
from sqlalchemy.orm import mapped_column, DeclarativeBase, Mapped, relationship

UUID_PK = Annotated[UUID, mapped_column(primary_key=True)]
HOST_FK = Annotated[UUID, mapped_column(ForeignKey("host.id"))]
REQUEST_FK = Annotated[UUID, mapped_column(ForeignKey("request.id"))]


class Base(DeclarativeBase):
    type_annotation_map = {
        dict[str, Any]: JSON,
        dict[str, str]: JSON,
    }
    id: Mapped[UUID_PK] = mapped_column(default=uuid4)


class RequestState(enum.Enum):
    CREATED = "created"
    PROCESSING = "processing"
    DONE = "done"
    ERROR = "error"


class HostSQLAlchemyModel(Base):
    __tablename__ = "host"
    __table_args__ = (UniqueConstraint("ip_address", "hostname"),)

    ip_address: Mapped[str]
    hostname: Mapped[str]
    requests: Mapped[list["RequestSQLAlchemyModel"]] = relationship(back_populates="host", cascade="all, delete-orphan")


class VulnerabilityClassSQLAlchemyModel(Base):
    __tablename__ = "vulnerability_class"
    __table_args__ = (
        UniqueConstraint("name", "description", "consequences", "recommendation", "impact_description"),
    )
    name: Mapped[str]
    description: Mapped[str]
    consequences: Mapped[str]
    recommendation: Mapped[str]
    impact_description: Mapped[str]
    request_id: Mapped[REQUEST_FK]
    request: Mapped["RequestSQLAlchemyModel"] = relationship(back_populates="vulnerabilities")


class ResponseSQLAlchemyModel(Base):
    __tablename__ = "response"
    __table_args__ = (UniqueConstraint("request_id"),)

    request_id: Mapped[REQUEST_FK]
    status_code: Mapped[int]

    cookies: Mapped[Optional[dict[str, Any]]] = mapped_column()
    mime_type: Mapped[Optional[str]] = mapped_column()
    headers: Mapped[Optional[dict[str, Any]]] = mapped_column()
    body: Mapped[Optional[bytes]] = mapped_column()
    request: Mapped["RequestSQLAlchemyModel"] = relationship(back_populates="response", single_parent=True)


class RequestSQLAlchemyModel(Base):
    __tablename__ = "request"

    host_id: Mapped[HOST_FK]
    method: Mapped[Literal['get', 'post', 'put', 'delete', 'options', 'head', 'patch', 'trace']]
    url: Mapped[str]
    # optional parameters
    state: Mapped[RequestState] = mapped_column(default=RequestState.CREATED)
    execution_date: Mapped[Optional[datetime]] = mapped_column()

    headers: Mapped[Optional[dict[str, Any]]] = mapped_column()
    cookies: Mapped[Optional[dict[str, Any]]] = mapped_column()
    query_parameters: Mapped[Optional[dict[str, Any]]] = mapped_column()
    path_parameters: Mapped[Optional[dict[str, Any]]] = mapped_column()
    body: Mapped[Optional[bytes]] = mapped_column()

    # relationship

    host: Mapped[HostSQLAlchemyModel] = relationship(back_populates="requests")
    response: Mapped["ResponseSQLAlchemyModel"] = relationship(back_populates="request", cascade="all, delete-orphan")
    vulnerabilities: Mapped[list["VulnerabilityClassSQLAlchemyModel"]] = relationship(back_populates="request",
                                                                                      cascade="all, delete-orphan")
